package com.moghisfere.cmviews;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by moghis on 21/08/2017.
 */
public class TypefaceProvider {
    public static final int NORMAL=1;
    public static final int LIGHT=0;
    public static final int BOLD=2;

    public static Typeface IRANIANSANS_NORMAL;
    public static Typeface IRANIANSANS_BOLD;
    public static Typeface IRANIANSANS_LIGHT;

    public static Typeface getIraniansansNormal(Context context)
    {
        if(IRANIANSANS_NORMAL==null)
        {
            IRANIANSANS_NORMAL= Typeface.createFromAsset(context.getAssets(), "fonts/iranian_sans.ttf");
        }
        return IRANIANSANS_NORMAL;
    }
    public static Typeface getIraniansansBold(Context context)
    {
        if(IRANIANSANS_BOLD==null)
        {
            IRANIANSANS_BOLD= Typeface.createFromAsset(context.getAssets(), "fonts/iranian_sans_bold.ttf");
        }
        return IRANIANSANS_BOLD;
    }
    public static Typeface getIraniansansLight(Context context)
    {
        if(IRANIANSANS_LIGHT==null)
        {
            IRANIANSANS_LIGHT= Typeface.createFromAsset(context.getAssets(), "fonts/iranian_sans_light.ttf");
        }
        return IRANIANSANS_LIGHT;
    }
}
