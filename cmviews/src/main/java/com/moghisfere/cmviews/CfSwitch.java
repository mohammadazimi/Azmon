package com.moghisfere.cmviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.SwitchCompat;
import android.util.AttributeSet;

/**
 * Created by moghis on 21/08/2017.
 */
public class CfSwitch extends SwitchCompat {
    public CfSwitch(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupViews(attrs);
    }

    public CfSwitch(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupViews(attrs);
    }

    public CfSwitch(Context context) {
        super(context);
        setupViews(null);
    }

    private void setupViews(AttributeSet attr) {
        if(attr!=null)
        {
            TypedArray attribute=getContext().obtainStyledAttributes(attr,R.styleable.Cfviews);
            try {
                int font=attribute.getInteger(R.styleable.Cfviews_fonts,0);
                switch (font)
                {
                    case TypefaceProvider.LIGHT:
                        setTypeface(TypefaceProvider.getIraniansansLight(getContext()));
                        break;
                    case TypefaceProvider.BOLD:
                        setTypeface(TypefaceProvider.getIraniansansBold(getContext()));
                        break;
                    case TypefaceProvider.NORMAL:
                        setTypeface(TypefaceProvider.getIraniansansNormal(getContext()));
                        break;
                }
            }
            finally {
                invalidate();
                requestLayout();
                attribute.recycle();
            }

        }
    }
}
