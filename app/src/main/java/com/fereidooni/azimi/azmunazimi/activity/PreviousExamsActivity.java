package com.fereidooni.azimi.azmunazimi.activity;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.fereidooni.azimi.azmunazimi.DataFakeGenerator;
import com.fereidooni.azimi.azmunazimi.R;
import com.fereidooni.azimi.azmunazimi.adaptor.ExamResultRecyclerViewAdaptor;

public class PreviousExamsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previous_exams);

        setUpToolbar();

        RecyclerView recyclerView = findViewById(R.id.recycler_view_previous_exams);
        ExamResultRecyclerViewAdaptor adaptor = new ExamResultRecyclerViewAdaptor(this,
                DataFakeGenerator.getPreviousExamList());
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        recyclerView.setAdapter(adaptor);
    }

    private void setUpToolbar(){
        Toolbar toolbar = findViewById(R.id.toolbar_previous_exams);
        setSupportActionBar(toolbar);

        //To make a back bottom in toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/iranian_sans.ttf");
        for (int i = 0; i < toolbar.getChildCount(); i++) {
            if (toolbar.getChildAt(i) instanceof TextView) {
                ((TextView) toolbar.getChildAt(i)).setTypeface(typeface);
            }
        }
    }
}
