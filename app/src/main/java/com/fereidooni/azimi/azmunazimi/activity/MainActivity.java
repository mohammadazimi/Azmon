package com.fereidooni.azimi.azmunazimi.activity;

import android.graphics.Typeface;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

import com.fereidooni.azimi.azmunazimi.R;
import com.fereidooni.azimi.azmunazimi.fragment.CollectionFragment;
import com.fereidooni.azimi.azmunazimi.fragment.MainFragmentAdapter;
import com.fereidooni.azimi.azmunazimi.fragment.StarterFragment;


public class MainActivity extends AppCompatActivity {
    private StarterFragment starterFragment;
    private CollectionFragment collectionFragment;
    private MainFragmentAdapter adapter;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main);
        adapter = new MainFragmentAdapter(getSupportFragmentManager());

        setUpToolbar();

        starterFragment = new StarterFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.fade_in,R.anim.run_out);
        transaction.add(R.id.fragment_container, starterFragment);
        transaction.commit();

    }

    public void replace(android.support.v4.app.Fragment fragment)
    {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.commit();
    }

    public void replaceAddToStack(android.support.v4.app.Fragment fragment)
    {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.run_in,R.anim.run_out);
        transaction.replace(R.id.fragment_container, fragment).addToBackStack(null);
        transaction.commit();
    }
/*
    public void back()
    {
        getSupportFragmentManager().popBackStack();
        getCurrentFragment();
    }
*/
    @Override
    public void onBackPressed() {
        //Log.d("backkk", "onBackPressed: "+getSupportFragmentManager().getBackStackEntryCount());
        if(getSupportFragmentManager().getBackStackEntryCount()>0)
            getSupportFragmentManager().popBackStack();
        else if (doubleBackToExitPressedOnce)
        {
            super.onBackPressed();
            return;
        }
        else {
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "برای خروج دوباره دکمه بازگشت را فشار دهید.", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 1000);
        }
    }



    private void setUpToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar_starter);
        setSupportActionBar(toolbar);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/iranian_sans.ttf");
        for (int i = 0; i < toolbar.getChildCount(); i++) {
            if (toolbar.getChildAt(i) instanceof TextView) {
                ((TextView) toolbar.getChildAt(i)).setTypeface(typeface);
            }
        }
    }
}
