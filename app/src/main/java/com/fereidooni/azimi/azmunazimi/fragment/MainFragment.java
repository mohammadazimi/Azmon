package com.fereidooni.azimi.azmunazimi.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.fereidooni.azimi.azmunazimi.R;

public class MainFragment extends Fragment {
    private RelativeLayout fragmentContainer;
    public static final int REQUEST_CODE = 3282;

    public MainFragment() {
        // Required empty public constructor
    }


    public static MainFragment newInstance(int index) {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putInt("index", index);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getArguments().getInt("index",0) == 0)
        {
            View view =  inflater.inflate(R.layout.fragment_thematic, container, false);
            initThematicView(view);
            return view;
        }
        else if (getArguments().getInt("index",0) == 1)
        {
            View view =  inflater.inflate(R.layout.fragment_comprehensive, container, false);
            initComprehensiveView(view);
            return view;
        }
        else if (getArguments().getInt("index",0) == 2)
        {
            View view =  inflater.inflate(R.layout.fragment_personal, container, false);
            initPersonalView(view);
            return view;
        }
        else
        {
            View view =  inflater.inflate(R.layout.fragment_progress , container, false);
            initProgressView(view);
            return view;
        }
    }

    private void initThematicView(View view) {
        fragmentContainer = view.findViewById(R.id.fragment_container);
    }

    private void initComprehensiveView(View view) {
        fragmentContainer = view.findViewById(R.id.fragment_container);
    }

    private void initPersonalView(View view) {
        fragmentContainer = view.findViewById(R.id.fragment_container);
    }

    private void initProgressView(View view) {
        fragmentContainer = view.findViewById(R.id.fragment_container);
    }

    /*public void refresh() {
        if (getArguments().getInt("index", 0) > 0 && recyclerView != null) {
            recyclerView.smoothScrollToPosition(0);
}
    }*/

    public void willBeDisplayed() {
        // Do what you want here, for example animate the content
        if (fragmentContainer != null) {
            Animation fadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
            fragmentContainer.startAnimation(fadeIn);
        }
    }

    public void willBeHidden() {
        if (fragmentContainer != null) {
            Animation fadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
            fragmentContainer.startAnimation(fadeOut);
        }
    }
}
