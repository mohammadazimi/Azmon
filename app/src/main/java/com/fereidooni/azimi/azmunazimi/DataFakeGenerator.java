package com.fereidooni.azimi.azmunazimi;

import android.content.Context;

import com.fereidooni.azimi.azmunazimi.model.PreviousExam;

import java.util.ArrayList;
import java.util.List;

public class DataFakeGenerator {
    public static List<PreviousExam> getPreviousExamList(){
        List<PreviousExam> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            PreviousExam exam = new PreviousExam();
            exam.setExamID(i);
            if (i%2 == 0){
                exam.setExamType("آزمون جامع");
            }
            else{
                exam.setExamType("آزمون موضوعی");
            }
            exam.setExamNumber("آزمون شماره " + (i+1));
            exam.setExamDate("تاریخ آزمون: " + "1397/ 08/ " + (i+12));
            exam.setExamResult("در صد کل: " + "%" +(50+(i*5)));
            list.add(exam);
        }
        return list;
    }
}
